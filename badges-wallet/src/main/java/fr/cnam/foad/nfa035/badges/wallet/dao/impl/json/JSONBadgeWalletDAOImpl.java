package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Set;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badges digitaux multiples
 * et PAR ACCES DIRECT, donc prenant en compte les métadonnées de chaque badges.
 */
public class JSONBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOImpl.class);

    private final File walletDatabase;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public JSONBadgeWalletDAOImpl(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence addBadge(DigitalBadge badge)
     * @hidden
     * @param image
     * @throws IOException
     */
    @Override
    @Deprecated
    public void addBadge(File image) throws IOException{
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }

    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException{
        try(JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))){
            ImageStreamingSerializer serializer = new JSONWalletSerializerDAImpl(getWalletMetadata("rw"));
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence getBadgeFromMetadata
     *
     * @param imageStream
     * @throws IOException
     */
    @Override
    @Deprecated
    public void getBadge(OutputStream imageStream) throws IOException{
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }


    /**
     * {@inheritDoc}
     * @return List<DigitalBadgeMetadata>
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        return getWalletMetadata("r");
    }

    /**
     * Permet à une autre méthode rw de s'appuyer dessus
     * @return List<DigitalBadgeMetadata>
     */
    private Set<DigitalBadge> getWalletMetadata(String mode) throws IOException {
        try(WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, mode))) {
            return new MetadataDeserializerJSONImpl().deserialize(media);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream,  DigitalBadgeMetadata meta ) throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try(WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new JSONWalletDeserializerDAImpl(imageStream, metas).deserialize(media, meta);
        }
    }
    public SortedMap<DigitalBadge, DigitalBadgeMetadata> getWalletMetadataMap() throws IOException {

    }

}
