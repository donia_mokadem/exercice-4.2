package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public interface JSONBadgeWalletDAO {
    void getBadgeFromMetadata(OutputStream imageStream , DigitalBadgeMetadata meta){
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try(WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new JSONWalletDeserializerDAImpl(imageStream, metas).deserialize(media, meta);
    }
}
    @Test
    public void testGetBadgeFromDatabaseByMetadataWithFraud(){
        try {
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("SUPER_STAR", begin, end, new DigitalBadgeMetadata(1, 0,557), null);

            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            assertThrows(IOException.class, () -> {
                dao.getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            });

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }
}
