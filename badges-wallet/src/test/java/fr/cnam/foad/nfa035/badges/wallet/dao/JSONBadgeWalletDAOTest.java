package fr.cnam.foad.nfa035.badges.wallet.dao;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class JSONBadgeWalletDAOTest {

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.json");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabaseException() {

        try {

            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");

            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge);

            assertThrows(IOException.class, () -> {
                dao.addBadge(badge);
            });

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabase(){

        try {

            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge1);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            DigitalBadge badge2 = new DigitalBadge("NFA034", begin, end, null, image2);
            dao.addBadge(badge2);

            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
            dao.addBadge(badge3);

            try (BufferedReader reader = new BufferedReader(new FileReader(walletDatabase))) {

                JsonFactory jsonFactory = new JsonFactory();
                jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
                ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

                String serializedImage = reader.readLine();
                LOG.info("1ère ligne:\n{}", serializedImage);
                DigitalBadge serializedBadge1 = objectMapper.readValue(serializedImage.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

                String serializedImage2 = reader.readLine();
                LOG.info("2ème ligne:\n{}", serializedImage2);
                DigitalBadge serializedBadge2 = objectMapper.readValue(serializedImage2.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);

                String serializedImage3 = reader.readLine();
                LOG.info("3ème ligne:\n{}", serializedImage3);
                DigitalBadge serializedBadge3 = objectMapper.readValue(serializedImage3.split(",\\{\"payload")[0].split(".*badge\":")[1], DigitalBadge.class);


                // Assertions
                assertEquals(badge1, serializedBadge1);
                assertEquals(badge2, serializedBadge2);
                assertEquals(badge3, serializedBadge3);
            }

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }


    /**
     * Teste la récupération des Métadonnées
     */
    @Test
    void testGetMetadata() throws IOException {
        DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");
        Set<DigitalBadge> metaSet = dao.getWalletMetadata();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());

        assertEquals(3, metaSet.size());
        Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
        assertEquals(new DigitalBadgeMetadata(1, 0,557), it.next().getMetadata());
        assertEquals(new DigitalBadgeMetadata(2,895, 906), it.next().getMetadata());
        assertEquals(new DigitalBadgeMetadata(3,2255, 35664), it.next().getMetadata());
    }

    /**
     * Test la récupération séquentielle des badges du wallet
     */
    @Test
    public void testGetBadgeFromDatabaseByMetadata(){
        try {
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(1, 0,557), null);
            DigitalBadge expectedBadge2 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(2, 895,906), null);
            DigitalBadge expectedBadge3 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(3, 2255,35664), null);
            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            dao.getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            dao.getBadgeFromMetadata(fileBadgeStream2, expectedBadge2);
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            dao.getBadgeFromMetadata(fileBadgeStream3, expectedBadge3);

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

}
